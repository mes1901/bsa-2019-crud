<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'my_products';
    protected $primaryKey = 'id';
    protected $attributes = [
        'available' => false,
    ];

    protected $fillable = ['name','price','seller_id'];
    protected $guarded = ['available',];

    public function getPrice($productId){
        return $this->where('id',$productId)->first()->price;
    }

    public function getName($productId){
        return $this->where('id',$productId)->first()->name;
    }

    public function seller(){
        return $this->belongsTo(Seller::class);
    }

    public function orderItems(){
        return $this->hasMany(OrderItem::class, 'product', 'id');
    }
}
