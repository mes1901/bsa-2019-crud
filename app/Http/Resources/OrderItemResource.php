<?php

namespace App\Http\Resources;

use App\Product;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderItemResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'productName' => (new Product())->getName($this->product),
            'productQty' => $this->quantity,
            'productPrice' => $this->price/100,
            'productDiscount' => $this->discount,
            'productSum' => Round(($this->price - $this->price * $this->discount/100) * $this->quantity)/100
        ];
    }
}
