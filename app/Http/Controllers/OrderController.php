<?php

namespace App\Http\Controllers;

use App\Buyer;
use App\Http\Resources\BuyerResource;
use App\Http\Resources\OrderItemResource;
use App\Http\Resources\OrderResource;
use App\Order;
use App\OrderItem;
use App\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Input;

class OrderController extends Controller
{

    public function index()
    {
        //
    }

    public function store(Request $request)
    {
        $data = Input::all();
        $date = Carbon::now()->toDateTimeString();
        $order = Order::create([
            'date' => $date,
            'buyer' => $data['buyer']
        ]);
        $order->save();

        $orderItem = [];
        if ($order) {
            $dataForCalc = app()->make('stdClass');
            for ($i = 0; $i < count($data['orderItems']); $i++) {
                $newData = [];
                foreach ($data['orderItems'][$i] as $key => $value) {
                    if ($key === 'product') {
                        if (!Product::find($value)) {
                            return new Response([
                                'result' => 'fail',
                                'message' => 'product with id: ' . $value . ' not found'
                            ]);
                        }
                        $dataForCalc->price = (new Product())->getPrice($value);
                        $newData['price'] = $dataForCalc->price;
                        $newData['order'] = $order['id'];
                    } elseif ($key === 'quantity') {
                        $dataForCalc->quantity = $value;
                    } elseif ($key === 'discount') {
                        $dataForCalc->discount = $value;
                    }
                    $newData[$key] = $value;
                }
                $newData['sum'] = (new OrderItem())->sumCalc($dataForCalc);
                $orderItem[$i] = OrderItem::create($newData);
                $orderItem[$i]->save();
            }
        }
        return new Response([$order, $orderItem]);
    }

    public function show($id)
    {
        $order = Order::find($id);
        if ($order) {
            $orderItems = (new OrderItem())->where('order', $id)->get();
            $orderSum = 0;
            for($i = 0; $i < count($orderItems); $i++){
                $orderSum += $orderItems[$i]->sum;
            }
            $order->orderSum = $orderSum;
            $orderResponse = new OrderResource($order);
            $itemsResponse = new OrderItemResource($orderItems);
            $buyerResponse = new BuyerResource(Buyer::find($orderResponse->buyer));
            return new Response([
                $orderResponse,
                $itemsResponse->map(function ($orderItem) {
                    return new OrderItemResource($orderItem);
                }),
                $buyerResponse
            ]);
        }
        return new Response([
            'result' => 'fail',
            'message' => 'order with id: ' . $id . ' not found'
        ]);
    }


    public function update(Request $request, $id)
    {
        $orderItems = OrderItem::where('order', $id)->get();

        if (!$orderItems) {
            return new Response([
                'result' => 'fail',
                'message' => 'order not found'
            ]);
        }

        $data = Input::all();

        $dataForCalc = app()->make('stdClass');
        for ($i = 0; $i < count($data['orderItems']); $i++) {
            foreach ($data['orderItems'][$i] as $key => $value) {
                if ($key === 'product') {
                    if (!Product::find($value)) {
                        return new Response([
                            'result' => 'fail',
                            'message' => 'product with id: ' . $value . ' not found'
                        ]);
                    }
                    $dataForCalc->price = (new Product())->getPrice($value);
                } elseif ($key === 'quantity') {
                    $dataForCalc->quantity = $value;
                } elseif ($key === 'discount') {
                    $dataForCalc->discount = $value;
                }
                $orderItems[$i]->$key = $value;
            }
            $orderItems[$i]->price = $dataForCalc->price;
            $orderItems[$i]->sum = (new OrderItem())->sumCalc($dataForCalc);
            $orderItems[$i]->save();
        }
        return new Response($orderItems);
    }

    public function destroy($id)
    {
        //
    }
}
