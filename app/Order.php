<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';
    protected $primaryKey = 'id';

    protected $fillable = ['buyer', 'date'];

    public function getOrder($id){
        return $this->where('id', $id)->first();
    }

    public function orderItems()
    {
        return $this->hasMany(OrderItem::class, 'order', 'id');
    }

    public function order()
    {
        return $this->belongsTo(Buyer::class);
    }
}
