<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    protected $fillable = ['order','product','quantity','price','discount','sum'];

    public function sumCalc($data){
        return Round(($data->price - $data->price * $data->discount / 100) * $data->quantity);
    }

    public function order(){
        return $this->belongsTo(Order::class);
    }

    public function product(){
        return $this->belongsTo(Product::class);
    }
}
