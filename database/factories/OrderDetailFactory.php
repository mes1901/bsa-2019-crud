<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\OrderItem;
use Faker\Generator as Faker;

$factory->define(OrderItem::class, function (Faker $faker) {
    $quantity = $faker->numberBetween(1, 5);
    $price = $faker->numberBetween(500, 20000);
    $discount = $faker->numberBetween(5, 50);
    $sum = ($price - $price * $discount/100) * $quantity;
    return [
        'product' => \App\Product::inRandomOrder()->first()->id,
        'quantity' => $quantity,
        'price' => $price,
        'discount' => $discount,
        'sum' => Round($sum)
    ];
});
